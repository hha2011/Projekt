void setUP() {
  player = loadImage("player.png");
  star = loadImage("star.png");
  background = loadImage("game_background.jpeg");
  space_image = loadImage("space_image.jpeg");
  player.resize(width / 15, height / 10);
  star.resize(width / 15, height / 10);
  background.resize(width, height + height / 4);
  space_image.resize(width, height);
}


void drAW() {
  image(background, 0, 0);
  image(player, px, py);
  fill(0);
  for(int i = 0; i < 15; i++) {
    for(int o = 0; o < 10; o++) {
      if(tiles[1][o][i] == 1) rect(i * width / 15, o * height / 10, width / 15, height / 10);
      if(tiles[1][o][i] == 2) image(star, i * width / 15, o * height / 10);
    }
  }
}
