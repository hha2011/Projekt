void setUp() {
  setUP();
}

void drAw() {
  drAW();
  px += pxv;
  py += pyv;
  pxv *= 0.9;
  pyv += 0.3;
}

void keyPressed() {
  if (key == 'd' || keyCode == RIGHT) pxv += 0.3;
  if (key == 'a' || keyCode == LEFT) pxv -= 0.3;
  if ((key == ' ' || key == 'w' ||keyCode == UP) && isGrounded()) pyv = -10;
  if (key == 'e' && isGrounded()) {
    pxv = 10;
    pyv = -10;
  }
  if (key == 'q' && isGrounded()) {
    pxv = -10;
    pyv = -10;
  }
}
